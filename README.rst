..
   Copyright 2018-2019 Luc Chouinard lumostor@3x0.ca. All rights reserved.

============================================
pypartkeepr -- Python library for PartKeepr
============================================

|GPLv3 license| |Documentation Status| |Pipeline Status| |Coverage Status|

Copyright 2018-2019 Luc Chouinard lumostor@3x0.ca. All rights reserved.

See *LICENSE.txt* for further license information.

Description
===========

pypartkeepr is a python library for connecting to `PartKeepr <https://partkeepr.org>`_ HTTP/REST API -
`PartKeepr <https://partkeepr.org>`_ is an electronic part inventory management software.
It will permit you to deal with the Partkeepr database like add part/stock, create projects.

It is currently possible to create, modify, delete parts, projects, part categories,
storage locations. Other ressources are available but have not been tested.

Currently pypartkeepr is in an early stage, it has not been tested very much, the API
will change, use at your own risk.

.. note::

   It is highly recommended to backup your database, or use a test server, before
   using pypartkeepr for the first time. You will have to familiarize with the
   PartKeepr data structure, and how to use it.

Documentation may be found on `ReadTheDocs <https://pypartkeepr.readthedocs.io/en/latest/>`_



Requirements
============

* Python 3.7
* python 3.6 (with dataclasses backport - https://github.com/ericvsmith/dataclasses.git) <currently broken>

* pytz
* PyYAML
* requests
* simple_rest_client
* appdirs
* octopart (to be removed)
* bs4  (for pypk-import script example)


Configs
=======

The configs are stored in a file (~/.config/pypartkeepr/config on linux), use the script
pypk-config to create, modify it.

.. code-block:: bash

   user@host> pypk-config -s pk.example.com --opapikey 452345E5 -u user

Currently login to PartKeepr is not possible with this library, you have to login using
your browser then copy the PHPSESSID cookie, value and domain, and pass it to pypk-login.
This will put the PHPSESSID cookie in the cookie jar in ~/.config/pypartkeepr/cookies on
linux.

.. code-block:: bash

   user@host> pypk-login 2p3j9tyitt3lrgshspcj6s779o pk.example.com
   
     
Common use cases
================


.. code-block:: python

   import pypartkeeper as pypk

   # The configs and login have already been done and put in files
   pk = pypk.PartKeepr()
   # this will list parts
   for p in pk.parts.values():
       print(p.id, p.name, p.category.name, p.storageLocation.name)

   # get a part by id, 10 being its id
   part = pk.parts[10]
		
   # print some info
   print(part.name, part.stockLevel, part.averagePrice)
		
   # add a comment
   part.comment = 'This part hard to find'
		
   #Update the database. If the part is present in DB then it will be updated else created
   pk.parts.add(part)
   # or use dict like interface; the id must be in the database
   pk.parts[10] = part
   # or it could have been updated like this. The id will come from
   # part.id; so part must have an id i.e. comming from the database
   pk.parts[part] = part
   # or also this way
   pk.parts[part.id] = part

   # add a new part (category, storageLocation, partUnit and name must be provided)
   cat = pk.part_categories.get_name('MOSFET')
   sl = pk.storage_locations.get_name('FELI-HOME-A012')
   punit = pk.part_measurement_units.get_name('pieces')
   # create the part instance, all other fields will be set to None
   newpart = pypk.Part.from_dict({'name': '2N7000', 'category': cat,
                                  'storageLocation': sl, 'partUnit': punit})
   # will return the part updated from the DB with the id setted
   db_newpart = pk.parts.add(newpart)
   # or but don't return the part from DB with the id set
   pk.parts[None] = newpart

   # also get a part from it's name
   part = pk.parts.get_name('INA230AIRGTR')

   # Or search
   # this will return a list generator
   # the operator is an SQL comparison operator, with % as the wildcard
   for p in pk.parts.search('name','LIKE','ICE%'):
       print(p)

       

.. |GPLv3 license| image:: https://img.shields.io/badge/License-GPLv3-blue.svg
   :target: https://gitlab.com/lumostor/pypartkeepr/blob/master/LICENSE.txt

.. |Documentation Status| image:: https://readthedocs.org/projects/pypartkeepr/badge/
   :target: https://readthedocs.org/projects/pypartkeepr

.. |Pipeline Status| image:: https://gitlab.com/lumostor/pypartkeepr/badges/master/pipeline.svg
   :target: https://gitlab.com/lumostor/pypartkeepr/commits/master

.. |Coverage Status| image:: https://gitlab.com/lumostor/pypartkeepr/badges/master/coverage.svg
   :target: https://gitlab.com/lumostor/pypartkeepr/commits/master

