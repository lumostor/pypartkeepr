# -*- mode: python; mode: elpy; coding: utf-8 -*-

import logging
from pkg_resources import get_distribution, DistributionNotFound

from .core import *
from .dataclasses import *
from .exceptions import *
from .utils import *

#logging.getLogger(__name__).addHandler(logging.NullHandler())

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = __name__
    __version__ = get_distribution(dist_name).version
except DistributionNotFound:
    __version__ = 'unknown'
finally:
    del get_distribution, DistributionNotFound
