# -*- mode: python; mode: elpy; coding: utf-8 -*-

#
# Copyright 2018 Luc Chouinard lumostor@3X0.ca
#
# This file is part of pypartkeepr.
#
# pypartkeepr is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pypartkeepr is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pypartkeepr.  If not, see <https://www.gnu.org/licenses/>.
#


def default_actions(resource_name):
    return {
        'list': {
            'method': 'GET',
            'url': resource_name
        },
        'create': {
            'method': 'POST',
            'url': resource_name
        },
        'get': {
            'method': 'GET',
            'url': resource_name + '/{}',
        },
        'update': {
            'method': 'PUT',
            'url': resource_name + '/{}',
        },
        'delete': {
            'method': 'DELETE',
            'url': resource_name + '/{}',
        }
    }


ACTIONS = {
    'octopart': {
        'actions': {
            #  http://localhost/api/octopart/get/8f734534f03350b4
            'get': {'method': 'GET', 'url': 'octopart/get/{}'},
            #  http://localhost/api/octopart/query/?q=NE555P&page=1
            'query': {'method': 'GET', 'url': 'octopart/query'},
        },
        'type': 'OctoPart',
    },

    'apidoc': {
        'actions': {
            'get': {'method': 'GET', 'url': 'apidoc'},
        },
        'type': 'apidoc',
    },

    'parts': {
        'actions': {
            # @resource.part part
            'create': {'method': 'POST', 'url': 'parts'},
            # @resource.part parts
            'list': {'method': 'GET', 'url': 'parts'},
            # @resource.part @resource.part
            'get': {'method': 'GET', 'url': 'parts/{}'},
            # @resource.part part
            'update': {'method': 'PUT', 'url': 'parts/{}'},
            # @resource.part @resource.part
            'delete': {'method': 'DELETE', 'url': 'parts/{}'},
            # @resource.part part
            'add_stock': {'method': 'PUT', 'url': 'parts/{}/addStock'},
            # @resource.part part
            'remove_stock': {'method': 'PUT', 'url': 'parts/{}/removeStock'},
            # @resource.part part
            'set_stock': {'method': 'PUT', 'url': 'parts/{}/setStock'},
            'mass_remove_stock': {'method': 'POST', 'url': 'parts/massRemoveStock'},
        },
        'type': 'Parts',
    },

    'part_categories': {'actions': default_actions('part_categories',),
                        'type': 'PartCategory', },
    'storage_locations': {'actions': default_actions('storage_locations',),
                          'type': 'StorageLocation', },

    'storage_location_categories': {
        'actions': default_actions('storage_location_categories',),
        'type': 'StorageLocationCategory',
    },
    'projects': {'actions': default_actions('projects'),
                 'type': 'Project', },
    'project_parts': {'actions': default_actions('project_parts'),
                      'type': 'ProjectPart', },
    'part_measurement_units': {'actions': default_actions('part_measurement_units'),
                               'type': 'PartMeasurementUnit', },
    'reports': {'actions': default_actions('reports'),
                'type': 'Report', },
    'projects_runs': {'actions': default_actions('projects_runs'),
                      'type': 'ProjectsRuns', },
    'stock_entries': {'actions': default_actions('stock_entries'),
                      'type': 'StockEntry', },
    'system_notices': {'actions': default_actions('system_notices'),
                       'type': 'SystemNotice', },
    'system_information': {'actions': default_actions('system_information'),
                           'type': 'SystemInformation', },
    'footprints': {'actions': default_actions('footprints'),
                   'type': 'Footprint', },
    'footprint_categories': {'actions': default_actions('footprint_categories'),
                             'type': 'FootprintCategory', },
    'manufacturers': {'actions': default_actions('manufacturers'),
                      'type': 'Manufacturer', },
    'distributors': {'actions': default_actions('distributors'),
                     'type': 'Distributor', },
    'user_providers': {'actions': default_actions('user_providers'),
                       'type': 'UserProviders', },
    'users': {
        'actions': {
            # @resource.user user_preference
            'get_preferences': {'method': 'GET', 'url': 'user_preferences'},
            # @resource.user user_preference
            'set_preference': {'method': 'POST', 'url': 'user_preferences'},
            # @resource.user user_preference
            'delete_preference': {'method': 'DELETE', 'url': 'user_preferences'},
            # @resource.user auth
            'login': {'method': 'POST', 'url': 'users/login'},
            # @resource.user auth
            'change_password': {'method': 'PUT', 'url': 'users/{}/changePassword'},
            # @resource.user auth
            'get_providers': {'method': 'GET', 'url': 'users/get_user_providers'},
            # @resource.user @resource.user
            'get': {'method': 'GET', 'url': 'users/{}'},
            # @resource.user user
            'update': {'method': 'PUT', 'url': 'users/{}'},
            # @resource.user user
            'create': {'method': 'POST', 'url': 'users'},
            # @resource.user user
            'delete': {'method': 'DELETE', 'url': 'users/{}'},
        },
        'type': 'Users',
    },



}
