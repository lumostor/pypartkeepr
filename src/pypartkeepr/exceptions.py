# -*- mode: python; mode: elpy; coding: utf-8 -*-

#
# Copyright 2018 Luc Chouinard lumostor@3X0.ca
#
# This file is part of pypartkeepr.
#
# pypartkeepr is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pypartkeepr is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pypartkeepr.  If not, see <https://www.gnu.org/licenses/>.
#

import re
from collections import Mapping

import simple_rest_client.exceptions


def exception_handler(e):
    if isinstance(e, simple_rest_client.exceptions.ServerError):
        if isinstance(e.response.body, Mapping):
            body = e.response.body['hydra:description']
        else:
            body = e.response.body

        if re.match(
            '^Type error: Argument 1 passed to Par.*, null given,.*',
                body):
            raise KeyError(
                'Key Not Found {}'.format(
                    re.sub(
                        '.*:\d\d',
                        '',
                        e.response.url))) from None

    raise e


class ConfigFileNotFound(Exception):
    pass


class CookiesFileNotFound(Exception):
    pass
