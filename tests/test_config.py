# -*- mode: python; mode: elpy; coding: utf-8 -*-

import unittest
from pathlib import Path

import yaml

from pypartkeepr.core import PartKeepr
from pypartkeepr.dataclasses import pyPKConfig

config_data_txt = '!pyPKConfig {log_level: DEBUG, octopart_api_key: E8AAA71E, port: 80, protocol: http, servername: localhost, username: test}'
config_data = pyPKConfig(protocol='http', servername='localhost', port=80, username='test', log_level='DEBUG', octopart_api_key='E8AAA71E')

test_config_file = Path('test_config')
test_config_file2 = Path('test_config2')


class TestConfig(unittest.TestCase):
    def setUp(self):
        self.pk = PartKeepr()
        with test_config_file.open('w') as tf:
            yaml.dump(config_data, tf)
        with test_config_file2.open('w') as tf:
            yaml.dump(config_data, tf)
        self.pk.config_file = test_config_file

    def tearDown(self):
        test_config_file.unlink()
        test_config_file2.unlink()

    def test_load(self):
        self.pk.load_config()
        self.assertEqual(self.pk.conf, yaml.safe_load(config_data_txt))

    def test_load_file(self):
        self.pk.load_config(file='test_config2')
        self.assertEqual(self.pk.conf, yaml.safe_load(config_data_txt))


if __name__ == '__main__':
    unittest.main()
