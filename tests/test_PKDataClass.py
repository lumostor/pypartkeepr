# -*- mode: python; mode: elpy; coding: utf-8 -*-

#
# Copyright 2018-2019 Luc Chouinard lumostor@3X0.ca
#
# This file is part of pypartkeepr.
#
# pypartkeepr is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pypartkeepr is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pypartkeepr.  If not, see <https://www.gnu.org/licenses/>.
#

import unittest

from pypartkeepr.dataclasses import PartCategory, FootprintCategory, Footprint


TEST2_CAT_DICT = {'@id': '/api/part_categories/3', '@type': 'PartCategory',
                  'parent': '/api/part_categories/2',
                  'children': [],
                  'categoryPath': 'Root Category ➤ test1 ➤ test2', 'expanded': True,
                  'name': 'test2', 'description': ''}

TEST3_CAT_DICT = {'@id': '/api/part_categories/4', '@type': 'PartCategory',
                  'parent': '/api/part_categories/2',
                  'children': [],
                  'categoryPath': 'Root Category ➤ test1 ➤ test3', 'expanded': True,
                  'name': 'test3', 'description': ''}

TEST1_CAT_DICT = {'@id': '/api/part_categories/2', '@type': 'PartCategory',
                  'parent': '/api/part_categories/1',
                  'children': ['/api/part_categories/3', TEST3_CAT_DICT],
                  'categoryPath': 'Root Category ➤ test1', 'expanded': True,
                  'name': 'test1', 'description': ''}

TEST5_CAT_DICT = {'@id': '/api/part_categories/5', '@type': 'PartCategory',
                  'parent': {'@id': '/api/part_categories/1', '@type': 'PartCategory',
                             'children': ['/api/part_categories/2'],
                             'categoryPath': 'Root Category',
                             'expanded': True, 'name': 'Root Category',
                             'description': ''},
                  'children': None, 'expanded': True, 'description': ''}

TEST_FP_CAT_DICT = {'@id': '/api/footprint_categories/1', '@type': 'FootprintCategory',
                    'children': [], 'categoryPath': 'Root Category', 'expanded': True,
                    'name': 'Root Category', 'description': ''}


class TestPKDataClass(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None
        self.root_cat = PartCategory('/api/part_categories/1', 'PartCategory', None, [],
                                     'Root Category', True, 'Root Category', '')
        self.test2_cat = PartCategory('/api/part_categories/2', 'PartCategory', None, [],
                                      'Root Category ➤ test1', True, 'test1', '')
        self.test3_cat = PartCategory('/api/part_categories/3', 'PartCategory', None, [],
                                      'Root Category ➤ test1 ➤ test2', True, 'test2', '')
        self.test4_cat = PartCategory('/api/part_categories/4', 'PartCategory', None, [],
                                      'Root Category ➤ test1 ➤ test3', True, 'test3', '')
        self.root_cat.children.append(self.test2_cat.id)
        self.test2_cat.children.append(self.test3_cat.id)
        self.test2_cat.children.append(self.test4_cat)
        self.test2_cat.parent = self.root_cat.id
        self.test3_cat.parent = self.test2_cat.id
        self.test4_cat.parent = self.test2_cat.id
        self.test5_cat = PartCategory('/api/part_categories/5', 'PartCategory',
                                      self.root_cat, [], None, True, None, '')
        self.test_FP_cat = FootprintCategory('/api/footprint_categories/1',
                                             'FootprintCategory', None, [],
                                             'Root Category', True, 'Root Category', '')

        # self.test_FP_attachment1 = FootprintAttachment('pdf
        self.test_FP = Footprint('/api/footprints/1', 'Footprint', 'test1',
                                 'a description', '/api/footprint_categories/1',
                                 None, [], 'Root Category ➤ test1')

    def test_from_dict_1(self):
        p = PartCategory.from_dict(TEST5_CAT_DICT)
        print('p        ', p)
        print('test5_cat', self.test5_cat)
        self.assertEqual(p, self.test5_cat)

    def test_from_dict2(self):
        p = PartCategory.from_dict(TEST1_CAT_DICT)
        self.assertEqual(self.test2_cat, p)

    def test_from_dict_3(self):
        fpc = FootprintCategory.from_dict(TEST_FP_CAT_DICT)
        self.assertEqual(self.test_FP_cat, fpc)

    def test_asdict(self):
        pd = self.test2_cat.asdict()
        self.assertEqual(TEST1_CAT_DICT, pd)


if __name__ == '__main__':
    unittest.main()
