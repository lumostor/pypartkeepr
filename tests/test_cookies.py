# -*- mode: python; mode: elpy; coding: utf-8 -*-

import unittest
from pathlib import Path

import yaml
import requests

from pypartkeepr.core import PartKeepr
# from pypartkeepr.dataclasses import *

cookies_data_txt = 'PHPSESSID: 2p3j9tyitt3lrgshspcj6s779o'
cookies_data = requests.utils.cookiejar_from_dict(
    yaml.safe_load(cookies_data_txt))


class TestCookies(unittest.TestCase):
    def setUp(self):
        self.pk = PartKeepr()
        with open('/tmp/test_cookies', 'w') as tf:
            yaml.dump(requests.utils.dict_from_cookiejar(cookies_data), tf)
        self.pk.cookies_file = Path('/tmp/test_cookies')
        self.pk.cookies = None

    def test_load(self):
        self.pk.load_cookies()
        self.assertEqual(self.pk.cookies, cookies_data)

    def test_load_file(self):
        self.pk.load_cookies(file='/tmp/test_cookies')
        self.assertEqual(self.pk.cookies, cookies_data)


if __name__ == '__main__':
    unittest.main()
